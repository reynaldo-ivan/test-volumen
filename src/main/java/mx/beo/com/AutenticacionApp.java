package mx.beo.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AutenticacionApp {

	public static void main(String[] args) {
		SpringApplication.run(AutenticacionApp.class, args);
	}
}
