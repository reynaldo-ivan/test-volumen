package mx.beo.com.api;
  
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
 

@RestController 
public class AutenticationController {
   
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/logs")
	public ResponseEntity<Object> cambioContrasena(RequestEntity<Object> requestEntity) {
		 
		Map<String, Object> map = (Map<String, Object>) requestEntity.getBody();
		String respuesta ="";
		String ruta= map.get("ruta").toString();
		System.out.println("La ruta del archivo------------"+ruta);
		try {
			respuesta = muestraContenido(ruta);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		System.out.println("respuesta-------------"+respuesta);
		
		return new ResponseEntity<Object>(respuesta, HttpStatus.OK); 
	}
	 
	
	 public String muestraContenido(String archivo) throws FileNotFoundException, IOException {
	        String cadena = null;
	        String todo = null;
	        FileReader f = new FileReader(archivo);
	        BufferedReader b = new BufferedReader(f);
	        while((cadena = b.readLine())!=null) {
	        	
	        	todo = todo + cadena; 
	        	System.out.println("bbb-------"+todo); 
	        }
	        b.close();
	        
	      return todo;  
	    }
 
	
	
}
